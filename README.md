# TIKI COMPOSER PACKAGES

[![pipeline status](https://gitlab.com/tikiwiki/tiki-packages-build/badges/master/pipeline.svg)](https://gitlab.com/tikiwiki/tiki-packages-build/commits/master)

## Overview

Also called the Composer Web Installer, and introduced in Tiki18, this feature is for the installation and management of external software packages using Composer
Composer is used in the Tiki code development process to import and manage external software such as jQuery and Bootstrap. But some software cannot be packaged with Tiki due to an incompatible license, or shouldn’t necessarily be packaged with Tiki by default because of the software’s specialized nature or niche application. So it is a natural step for Tiki site administrators to be able to use Composer to install and manage external software specifically for their site after the site is installed.

## Manual package installation

Since in some cases tiki administrators can't use the web interface to install the packages, this repository provides pre-packed packages ready to be manually uncompressed/uploaded to a Tiki instance.

To access the packages go to: [https://packages.tiki.org](https://packages.tiki.org)
