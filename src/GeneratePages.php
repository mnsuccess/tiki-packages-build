<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

namespace TikiPackages;

use TikiPackages\Command\BuildCommand;

class GeneratePages
{
	protected $resources;

	public function __construct()
	{
		$this->resources = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'resources';
	}

	public function generate($versionList, $publicFolder, $template = 'index')
	{
		$this->copyAssets($publicFolder);
		$this->generateIndex($versionList, $publicFolder, $template);
	}

	protected function copyAssets($publicFolder)
	{
		$destination = escapeshellarg($publicFolder);

		foreach (['css', 'img'] as $folder) {
			$source = escapeshellarg($this->resources . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . $folder);
			exec(sprintf('cp -r %s %s', $source, $destination));
		}
	}

	protected function generateIndex($versionList, $publicFolder, $template)
	{
		$packagesFolder = BuildCommand::PACKAGES_FOLDER;
		// local variables are exported to template ($versionList, $publicFolder)
		ob_start();
		include $this->resources . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $template .'.tpl.php';
		$html = ob_get_clean();

		file_put_contents($publicFolder . DIRECTORY_SEPARATOR . 'index.html', $html);
	}
}
