<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

namespace TikiPackages\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TikiPackages\GeneratePages;

class MaintenanceCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('maintenance')
            ->setDescription('Set Maintenance Mode')
            ->addOption(
                'public',
                'p',
                InputOption::VALUE_REQUIRED,
                'Public folder',
                'public'
            )
            ->addOption(
                'tmp',
                't',
                InputOption::VALUE_REQUIRED,
                'Temporary folder',
                '.tmp'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $publicFolder = $input->getOption('public');
        $tempFolder = $input->getOption('tmp');

        if (!is_dir($tempFolder)) {
            mkdir($tempFolder, 0755, true);
        }

        $pageGenerator = new GeneratePages();
        $pageGenerator->generate([], $publicFolder, 'maintenance');
    }
}
