<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

namespace TikiPackages;

class PackPackage
{
	protected $output;

	/**
	 * Package a Composer package as a tar
	 *
	 * @param string $packageName
	 * @param string $packageConfig
	 * @param string $destinationFolder
	 * @param string $temporaryFolder
	 *
	 * @return string
	 */
	public function pack($packageName, $packageConfig, $destinationFolder, $temporaryFolder)
	{
		$this->clearOutput();

		$resultFolderName = strtolower('tiki-pkg-' . $packageName);
		$version = preg_replace('/[^a-z0-9 .\-]/i', '', $packageConfig['requiredVersion']);
		$resultFileName = $resultFolderName .'-' . $version . '.zip';

		if (file_exists($destinationFolder . DIRECTORY_SEPARATOR . $resultFileName)) {
			$this->appendOutput('Re-using package at ' . $destinationFolder . DIRECTORY_SEPARATOR . $resultFileName);
			return $resultFileName;
		}

		$cwd = getcwd();

		$this->appendOutput('Packaging: ' . $packageName . "\n");

		$tmpDir = $temporaryFolder . DIRECTORY_SEPARATOR . uniqid('', true) . '-' . $packageName;
		mkdir($tmpDir, 0755, true);

		$this->appendOutput('Using Folder: ' . $tmpDir . "\n");

		chdir($tmpDir);

		$json = json_decode($this->getComposerJsonTemplate(), true);
		$json = $this->addComposerPackageToJson(
			$json,
			$packageConfig['name'],
			$packageConfig['requiredVersion'],
			isset($packageConfig['scripts']) ? $packageConfig['scripts'] : []
		);
		$jsonString = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
		file_put_contents('composer.json', $jsonString);
		$this->appendOutput('* Composer File:');
		$this->appendOutput($jsonString);

		$this->executeCommand('composer --no-ansi --no-dev --prefer-dist update nothing');

		$this->executeCommand('cp composer.json composer.lock vendor/');

		$this->executeCommand(sprintf('mv vendor %s', escapeshellarg($resultFolderName)));

		$this->executeCommand(
			sprintf('zip -r %s %s', escapeshellarg($resultFileName), escapeshellarg($resultFolderName))
		);

		chdir($cwd);

		rename(
			$tmpDir . DIRECTORY_SEPARATOR . $resultFileName,
			$destinationFolder . DIRECTORY_SEPARATOR . $resultFileName
		);

		return $resultFileName;
	}

	protected function executeCommand($command, $maxOutputLines = 100)
	{
		$this->appendOutput("* Executing: " . $command . "\n");
		exec($command . ' 2>&1', $output);
		if (count($output) > $maxOutputLines) {
			$output = array_merge(
				array_slice($output, 0, floor($maxOutputLines / 2)),
				['[... output omitted ...]'],
				array_slice($output, -floor($maxOutputLines / 2))
			);
		}
		$string = implode("\n", $output);
		if (substr($string, -1) != "\n") {
			$string .= "\n";
		}
		$this->appendOutput($string);
	}

	public function clearOutput()
	{
		$this->output = '';
	}

	public function getOutput()
	{
		return $this->output;
	}

	protected function appendOutput($output)
	{
		$this->output .= $output;
	}

	protected function getComposerJsonTemplate()
	{
		$template = <<<EOF
{
	"name": "tiki/tiki-custom",
	"description": "Tiki Wiki CMS Groupware",
	"license": "LGPL-2.1",
	"homepage": "https://tiki.org",
	"minimum-stability": "stable",
	"require": {
	},
	"config": {
		"process-timeout": 5000,
		"bin-dir": "vendor/bin"
	},
	"repositories": [
		{
			"type": "composer",
			"url": "https://composer.tiki.org"
		}
	]
}
EOF;
		return $template;
	}

	/**
	 * Append a package to composer.json
	 *
	 * @param $composerJson
	 * @param $package
	 * @param $version
	 * @param array $scripts
	 * @return array
	 */
	protected function addComposerPackageToJson($composerJson, $package, $version, $scripts = [])
	{

		$scriptsKeys = [
			'pre-install-cmd',
			'post-install-cmd',
			'pre-update-cmd',
			'post-update-cmd',
		];

		if (! is_array($composerJson)) {
			$composerJson = [];
		}
		// require
		if (! isset($composerJson['require'])) {
			$composerJson['require'] = [];
		}
		if (! isset($composerJson['require'][$package])) {
			$composerJson['require'][$package] = $version;
		}

		// scripts
		if (is_array($scripts) && count($scripts)) {
			if (! isset($composerJson['scripts'])) {
				$composerJson['scripts'] = [];
			}
			foreach ($scriptsKeys as $type) {
				if (! isset($scripts[$type])) {
					continue;
				}
				$scriptList = $scripts[$type];
				if (is_string($scriptList)) {
					$scriptList = [$scriptList];
				}
				if (! count($scriptList)) {
					continue;
				}
				if (! isset($composerJson['scripts'][$type])) {
					$composerJson['scripts'][$type] = [];
				}
				foreach ($scriptList as $scriptString) {
					$composerJson['scripts'][$type][] = $scriptString;
				}
				$composerJson['scripts'][$type] = array_unique($composerJson['scripts'][$type]);
			}
		}

		return $composerJson;
	}
}
