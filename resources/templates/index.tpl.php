<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

if (! isset($versionList) || ! is_array($versionList)) {
	echo "This file should be used as template, as part of the application, should not be called directly";
	return;
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TIKI Packages - for manual installation</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Anonymous+Pro:400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/styles.css">
	<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
</head>
<body>
<div class="header">
	<h6>Tiki Composer<br>Packages</h6><br>
	<div class="btns">
		<?php
		$counter = 1;
		foreach ($versionList as $version => $versionConfig) {
			?>
			<div class="btn btn0<?php echo $counter; ?>"><a href="#version0<?php echo $counter; ?>"><h4>
						TIKI <?php echo strtoupper($version); ?></h4></a></div>
			<?php
			$counter++;
		}
		?>
	</div>
</div>
<section class="description">
	<h1>Overview</h1>
	<h2>Also called the Composer Web Installer, and introduced in Tiki18, this feature is for the installation and
		management of external software packages using Composer</h2>
	<p>Composer is used in the Tiki code development process to import and manage external software such as jQuery and
		Bootstrap. But some software cannot be packaged with Tiki due to an incompatible license, or shouldn’t
		necessarily be packaged with Tiki by default because of the software’s specialized nature or niche application.
		So it is a natural step for Tiki site administrators to be able to use Composer to install and manage external
		software specifically for their site after the site is installed. </p>
	<p>You can read all the details directly from the Tiki documentation website at
		<strong><a href="https://doc.tiki.org/Packages">https://doc.tiki.org/Packages</a></strong>
	</p>

	<h1>How to use</h1>
	<p>Download from the list of packages below the one corresponding to your version of Tiki, currently there are
		packages provided for
        <?php
        $counter = 1;
        foreach ($versionList as $version => $versionConfig) {
        ?>
            <strong><a href="#version0<?php echo $counter; ?>" class="hover_version hover_version0<?php echo $counter; ?>">TIKI <?php echo strtoupper($version); ?></a></strong>
        <?php
            $counter++;
            if ($counter < count($versionList)) { echo ', '; }
            if ($counter == count($versionList)) { echo ' and '; }
        } ?></p>
	<p>After downloading the zip file, extract the zip file and copy the resulting folder to your Tiki installation,
		inside the folder "vendor_custom" that is located in the root folder of your Tiki installation, like the example
		below for the package "PdfJs"</p>
	<p class="center-text"><img src="img/pkg-example.png"/></p>
	<p>And it's done! Tiki will pick up the package automatically and you will be able to use it as soon as you enable
		the preference that requires that specific package.</p>
</section>
<?php
$counter = 1;
foreach ($versionList as $version => $versionConfig) {
	?>
	<section id="version0<?php echo $counter; ?>">
		<h1 class="version-title">TIKI <?php echo strtoupper($version); ?></h1>

		<div class="container container0<?php echo $counter; ?>">

			<?php
			foreach ($versionConfig['packages'] as $packageName => $packageConfig) {
				?>
				<div class="item">
					<div class="info">
						<h4><?php echo $packageName; ?></h4>
						<h3><span class="semibold">Licence:</span> <?php echo $packageConfig['licence']; ?></h3>
						<h3><span class="semibold">Name:</span> <?php echo $packageConfig['name']; ?></h3>
					</div>
					<div class="download">
						<a href="<?php echo $packagesFolder . '/' . $packageConfig['zipFile']; ?>">
							<img src="img/down-0<?php echo $counter; ?>.svg" style="height: 30px"></img>
							<h3>download .zip</h3></a>
					</div>
				</div>
			<?php } ?>
		</div>
	</section>
	<?php
	$counter++;
}
?>
</body>
<script type="text/javascript">
	$('a').click(function () {
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top
		}, 750);
		return false;
	});
</script>
</html>
